package br.com.mastertech.acesso.consumer;

import br.com.mastertech.acesso.io.CSVHandler;
import br.com.mastertech.acesso.model.Access;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class AccessConsumer {

    private static final Path path = Paths.get(System.getProperty("user.home"),"consumerLog.csv");

    @KafkaListener(topics = {"spec4-muniz-1", "spec4-muniz-2", "spec4-muniz-3"}, groupId = "testAccess")
    public void receber(@Payload Access acesso) throws Exception {
        System.out.println("Acesso recebido: " + acesso.toString());
        CSVHandler.csvWriterOneByOne(acesso, path);

    }
}
